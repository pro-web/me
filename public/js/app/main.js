(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "../../../node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!******************************************************************************************!*\
  !*** /Users/apple/Documents/MIN/me/node_modules/raw-loader!./src/app/app.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<!-- Start Header -->\n\n<section id=\"home-cont\" class=\"home1 home\">\n    <div class=\"zoominheader bg-image\">\n        <div class=\"overlay\"></div>\n        <div class=\"zoomoutheader\"></div>\n    </div>\n    <div class=\"title\">\n        <h1 class=\"text-uppercase\">Thanh<strong> Hai</strong></h1>\n        <p class=\"type\"></p> <!-- You Can Edit the title on the custom.js File -->\n        <div class=\"btn-home\">\n            <a href=\"#work-cont\">Portfolio</a>\n        </div>\n    </div>\n</section>\n\n<!-- End Header -->\n\n\n<!-- Start About -->\n<app-about></app-about>\n<!-- End About -->\n\n\n<!-- Start Services -->\n<app-services></app-services>\n<!-- End Services -->\n\n\n<!-- Start Work -->\n<app-portfolio></app-portfolio>\n<!-- End Work -->\n\n\n<!-- Start Testimonials -->\n<app-testimonials></app-testimonials>\n\n<!-- Start Contact -->\n<app-contact></app-contact>\n<!-- End Contact -->\n\n<!-- Blog -->\n<app-blog></app-blog>\n<!-- Start Loading -->\n\n<div class=\"loading\">\n    <div class=\"blob-grid\">\n        <div class=\"blob blob-0\"></div>\n        <div class=\"blob blob-1\"></div>\n        <div class=\"blob blob-2\"></div>\n        <div class=\"blob blob-3\"></div>\n        <div class=\"blob blob-4\"></div>\n        <div class=\"blob blob-5\"></div>\n    </div>\n</div>\n\n<!-- End Loading -->\n\n\n<div style=\"text-align: center;\">\n    <div style=\"position:relative; top:0; margin-right:auto;margin-left:auto; z-index:99999\">\n\n    </div>\n</div>"

/***/ }),

/***/ "../../../node_modules/raw-loader/index.js!./src/app/components/navbar/navbar.component.html":
/*!***************************************************************************************************************!*\
  !*** /Users/apple/Documents/MIN/me/node_modules/raw-loader!./src/app/components/navbar/navbar.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Start Navbar -->\n<nav class=\"navbar fixed-top navbar-expand-lg navbar-light\">\n\n    <div class=\"container\">\n        <div class=\"logo\">\n            <a href=\"#\">\n                <span class=\"bg_logo\"></span>\n            </a>\n        </div>\n\n        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\"\n            aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n            <div class=\"menu\">\n                <span class=\"line1\"></span>\n                <span class=\"line2\"></span>\n                <span class=\"line3\"></span>\n            </div>\n        </button>\n        <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\n            <ul class=\"navbar-nav ml-auto\">\n                <li id=\"home\" class=\"nav-item active-nav\">\n                    <a href=\"#\" data-value=\"home-cont\">\n                        <i class=\"icon-home\" hidden></i>\n                        <span class=\"name-menu\">Home</span>\n                    </a>\n                </li>\n                <li id=\"about\" class=\"nav-item\">\n                    <a href=\"#about-cont\" data-value=\"about-cont\">\n                        <i class=\"icon-male-user\" hidden></i>\n                        <span class=\"name-menu\">About</span>\n                    </a>\n                </li>\n                <li id=\"serv\" class=\"nav-item\">\n                    <a href=\"#serv-cont\" data-value=\"serv-cont\">\n                        <i class=\"icon-layers\" hidden></i>\n                        <span class=\"name-menu\">Services</span>\n                    </a>\n                </li>\n                <li id=\"work\" class=\"nav-item\">\n                    <a href=\"#work-cont\" data-value=\"work-cont\">\n                        <i class=\"icon-picture\" hidden></i>\n                        <span class=\"name-menu\">Portfolio</span>\n                    </a>\n                </li>\n                <li id=\"testim\" class=\"nav-item\">\n                    <a href=\"#testim-cont\" data-value=\"testim-cont\">\n                        <i class=\"icon-speech\" hidden></i>\n                        <span class=\"name-menu\">Testimonials</span>\n                    </a>\n                </li>\n                <li id=\"blog\" class=\"nav-item\">\n                    <a href=\"#blog-cont\" data-value=\"blog-cont\">\n                        <i class=\"icon-envelope\" hidden></i>\n                        <span class=\"name-menu\">Blog</span>\n                    </a>\n                </li>\n                <li id=\"contact\" class=\"nav-item\">\n                    <a href=\"#contact-cont\" data-value=\"contact-cont\">\n                        <i class=\"icon-envelope\" hidden></i>\n                        <span class=\"name-menu\">Contact</span>\n                    </a>\n                </li>\n            </ul>\n        </div>\n        <div class=\"social-media\">\n            <a href=\"https://www.facebook.com/\" target=\"_blank\"><i class=\"fab fa-facebook-f\"></i></a>\n            <!-- You Can Edit the link here -->\n            <a href=\"https://www.twitter.com/\" target=\"_blank\"><i class=\"fab fa-twitter\"></i></a>\n            <!-- You Can Edit the link here -->\n            <a href=\"https://www.tumblr.com/\" target=\"_blank\"><i class=\"fab fa-tumblr\"></i></a>\n            <!-- You Can Edit the link here -->\n            <a href=\"https://www.youtube.com/\" target=\"_blank\"><i class=\"fab fa-youtube\"></i></a>\n            <!-- You Can Edit the link here -->\n            <a href=\"https://www.instagram.com/\" target=\"_blank\"><i class=\"fab fa-instagram\"></i></a>\n            <!-- You Can Edit the link here -->\n        </div>\n        <div class=\"collapse-menu\">\n            <span class=\"collapse-icon closed-menu\"></span>\n        </div>\n    </div>\n</nav>\n<div class=\"nav-toggle\">\n    <div class=\"menu\">\n        <span class=\"line1\"></span>\n        <span class=\"line2\"></span>\n        <span class=\"line3\"></span>\n    </div>\n</div>\n<!-- End Navbar -->"

/***/ }),

/***/ "../../../node_modules/raw-loader/index.js!./src/app/modules/about/about.component.html":
/*!**********************************************************************************************************!*\
  !*** /Users/apple/Documents/MIN/me/node_modules/raw-loader!./src/app/modules/about/about.component.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"about-cont\" class=\"about\">\n  <div class=\"title\">\n    <h1 class=\"kaushan\">About</h1>\n  </div>\n  <div class=\"container\">\n\n    <div class=\"row\">\n      <div class=\"col-md-10 col-lg-6\">\n        <div class=\"about-img bg-image\"></div>\n        <!-- go to main.scss file to edit personal image -->\n      </div>\n      <div class=\"col-md-12 col-lg-6\">\n        <div class=\"tabs-about\">\n          <ul class=\"nav nav-tabs text-center\" id=\"myTab\" role=\"tablist\">\n\n            <!-- Start About Me Title Tab -->\n            <li class=\"nav-item\">\n              <a class=\"nav-link active\" id=\"about-tab\" data-toggle=\"tab\" href=\"#aboutme\" role=\"tab\"\n                aria-controls=\"about\" aria-expanded=\"true\">\n                <i class=\"icon-male-user\"></i>\n                <span>About me</span>\n              </a>\n            </li>\n            <!-- End About Me Title Tab -->\n\n            <!-- ================ -->\n\n            <!-- Start Profile Title Tab -->\n            <li class=\"nav-item\">\n              <a class=\"nav-link\" id=\"profile-tab\" data-toggle=\"tab\" href=\"#profile\" role=\"tab\" aria-controls=\"profile\">\n                <i class=\"icon-star\"></i>\n                <span>Profile</span>\n              </a>\n            </li>\n            <!-- End Profile Title Tab -->\n\n            <!-- ================ -->\n\n            <!-- Start Skills Title Tab -->\n            <li class=\"nav-item\">\n              <a class=\"nav-link\" id=\"skill-tab\" data-toggle=\"tab\" href=\"#skill\" role=\"tab\" aria-controls=\"skill\">\n                <i class=\"icon-magic-wand\"></i>\n                <span>Skills</span>\n              </a>\n            </li>\n            <!-- End Skills Title Tab -->\n          </ul>\n          <div class=\"tab-content\" id=\"myTabContent\">\n\n            <!-- Start About Me Tab -->\n            <div class=\"tab-pane fade show active text-left\" id=\"aboutme\" role=\"tabpanel\" aria-labelledby=\"about-tab\">\n              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt\n                ut labore et dolore magna aliqua labo ris nisi ut aliquip ex ea commodo consequat.</p>\n              <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat\n                nulla pariatur Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia\n                deserunt mollit anim id est laborum.</p>\n              <p>Hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur\n                aut perferendis doloribus asperiores repellat.</p>\n              <img src=\"assets/images/signature.png\" class=\"img-fluid signature\" alt=\"signature\" />\n              <!-- Here image Signature -->\n            </div>\n            <!-- End About Me Tab -->\n\n            <!-- ================ -->\n\n            <!-- Start Profile Tab -->\n            <div class=\"tab-pane fade text-left profile\" id=\"profile\" role=\"tabpanel\" aria-labelledby=\"profile-tab\">\n              <h6><span class=\"title-info\">Name :</span><span>Thanh Hai</span></h6>\n              <!-- Edit Your Name  -->\n              <h6><span class=\"title-info\">Nationality : </span><span>Viet Nam</span></h6>\n              <!-- Edit Your Nationality  -->\n              <h6><span class=\"title-info\">Email : </span><span>dangthithanhhai94itplus@gmail.com</span></h6>\n              <!-- Edit Your Email  -->\n              <h6><span class=\"title-info\">Date of birth : </span><span>05-09-1994</span></h6>\n              <!-- Edit Your Date Of Birth  -->\n              <h6><span class=\"title-info\">Phone : </span><span>+84 987 498 880</span></h6>\n              <!-- Edit Your Phone -->\n\n              <div class=\"btn-about\">\n                <a href=\"#\">Download Resume</a>\n              </div>\n            </div>\n            <!-- End Profile Tab -->\n\n            <!-- ================ -->\n\n            <!-- Start Skills Tab -->\n            <div class=\"tab-pane fade skill\" id=\"skill\" role=\"tabpanel\" aria-labelledby=\"skill-tab\">\n              <div class=\"skills\">\n                <p class=\"progress-text\">Typescript (Angular framework 2+)</p> <!-- Here You Can Edit The Name Of Skill -->\n                <div class=\"progress\">\n                  <div class=\"progress-bar\" data-present=\"80%\"></div>\n                  <!-- Here You Can Edit The Percentage -->\n                </div>\n                <p class=\"progress-text\">Business analyst</p> <!-- Here You Can Edit The Name Of Skill -->\n                <div class=\"progress\">\n                  <div class=\"progress-bar\" data-present=\"50%\"></div>\n                  <!-- Here You Can Edit The Percentage -->\n                </div>\n                <p class=\"progress-text\">Php (Laravel framework)</p> <!-- Here You Can Edit The Name Of Skill -->\n                <div class=\"progress\">\n                  <div class=\"progress-bar\" data-present=\"50%\"></div>\n                  <!-- Here You Can Edit The Percentage -->\n                </div>\n                <p class=\"progress-text\">Photoshop</p> <!-- Here You Can Edit The Name Of Skill -->\n                <div class=\"progress\">\n                  <div class=\"progress-bar\" data-present=\"50%\"></div>\n                  <!-- Here You Can Edit The Percentage -->\n                </div>\n              </div>\n              <div class=\"btn-about\">\n                <a href=\"#contact-cont\">Hire me</a>\n              </div>\n            </div>\n            <!-- End Skills Tab -->\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>"

/***/ }),

/***/ "../../../node_modules/raw-loader/index.js!./src/app/modules/blog/blog.component.html":
/*!********************************************************************************************************!*\
  !*** /Users/apple/Documents/MIN/me/node_modules/raw-loader!./src/app/modules/blog/blog.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"blog-cont\" class=\"blog\">\n    <div class=\"title\">\n        <h1 class=\"kaushan\">Blog</h1>\n    </div>\n    <div class=\"container\">\n        <div class=\"shuffle\">\n            <ul class=\"list-unstyled\">\n                <li data-class=\"all\" class=\"active item\">All</li>\n                <li data-class=\".web\" class=\"item\">Web Design</li>\n                <li data-class=\".graphic\" class=\"item\">Graphic Design</li>\n                <li data-class=\".marketing\" class=\"item\">Marketing</li>\n            </ul>\n        </div>\n        <div class=\"gallery\">\n            <div class=\"row\">\n                <!-- Start Image Number 1 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item marketing\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Marketing</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/01.jpg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/01.jpg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 1 -->\n                <!-- ===================== -->\n                <!-- Start Image Number 2 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item graphic\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Graphic Design</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/02.jpg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/02.jpg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 2 -->\n                <!-- ===================== -->\n                <!-- Start Image Number 3 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item web\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Web Design</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/03.jpeg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/03.jpeg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 3 -->\n                <!-- ===================== -->\n                <!-- Start Image Number 4 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item marketing\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Marketing</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/04.jpg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/04.jpg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 4 -->\n                <!-- ===================== -->\n                <!-- Start Image Number 5 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item web\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Web Design</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/05.jpg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/05.jpg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 5 -->\n                <!-- ===================== -->\n                <!-- Start Image Number 6 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item graphic\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Graphic Design</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/06.jpg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/06.jpg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 6 -->\n                <!-- ===================== -->\n                <!-- Start Image Number 7 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item marketing less\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Marketing</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/07.jpg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/07.jpg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 7 -->\n                <!-- ===================== -->\n                <!-- Start Image Number 8 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item graphic less\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Graphic Design</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/08.jpg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/08.jpg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 8 -->\n                <!-- ===================== -->\n                <!-- Start Image Number 9 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item web less\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Web Design</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/09.jpg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/09.jpg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 9 -->\n                <!-- You can add more images here.. -->\n            </div>\n        </div>\n    </div>\n</section>\n"

/***/ }),

/***/ "../../../node_modules/raw-loader/index.js!./src/app/modules/contact/contact.component.html":
/*!**************************************************************************************************************!*\
  !*** /Users/apple/Documents/MIN/me/node_modules/raw-loader!./src/app/modules/contact/contact.component.html ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"contact-cont\" class=\"contact\">\n    <div class=\"title\">\n        <h1 class=\"kaushan\">Contact</h1>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-md-6 col-sm-12\">\n            <div class=\"form\">\n                <form id=\"my_form\" method=\"post\" onsubmit=\"submitForm(); return false;\">\n                    <div class=\"row\">\n                        <div class=\"col-6\">\n                            <div class=\"form-group\">\n                                <input id=\"name\" class=\"form-control\" type=\"text\" name=\"name\" placeholder=\"Your Name\"\n                                    required />\n                            </div>\n                        </div>\n                        <div class=\"col-6\">\n                            <div class=\"form-group\">\n                                <input id=\"email\" class=\"form-control\" type=\"email\" name=\"email\"\n                                    placeholder=\"Your Email\" required />\n                            </div>\n                        </div>\n                        <div class=\"col-6\">\n                            <div class=\"form-group\">\n                                <input id=\"phone\" class=\"form-control\" type=\"tel\" name=\"phone\"\n                                    placeholder=\"Your Phone\" />\n                            </div>\n                        </div>\n                        <div class=\"col-6\">\n                            <div class=\"form-group\">\n                                <input id=\"subject\" class=\"form-control\" type=\"text\" name=\"subject\"\n                                    placeholder=\"Subject\" />\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"message\">\n                        <div class=\"form-group\">\n                            <textarea id=\"msg\" class=\"form-control\" placeholder=\"Write Your message here ...\"\n                                required></textarea>\n                        </div>\n                        <p>\n                            <span class=\"send-btn\">\n                                <input id=\"btn\" class=\"btn\" type=\"submit\" value=\"Send Message\" /><i\n                                    class=\"fas fa-long-arrow-alt-right\"></i>\n                            </span>\n                            <span id=\"status\"></span>\n                        </p>\n                    </div>\n                </form>\n            </div>\n        </div>\n        <div class=\"col-md-6 col-sm-12\">\n            <div class=\"infos background-gradient\">\n                <h2 class=\"text-uppercase kaushan\">Reva</h2>\n                <div class=\"email\">\n                    <i class=\"fas fa-envelope\"></i>\n                    <a href=\"mailto:reva@email.com\">reva@email.com</a>\n                </div>\n                <div class=\"phone\">\n                    <i class=\"fas fa-phone\"></i>\n                    <a href=\"tel:+1234567899\">+123 456 7899</a>\n                </div>\n                <div class=\"address\">\n                    <i class=\"fas fa-map-marker-alt\"></i>\n                    <span>123 Street Name, City , 32008</span>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"maps\" id=\"overlay\">\n        <div class=\"open-map\">\n            <span class=\"o-map active\"><i class=\"fas fa-arrow-alt-circle-down\"></i>Open Map</span>\n            <span class=\"c-map\"><i class=\"fas fa-arrow-alt-circle-up\"></i>Close Map</span>\n        </div>\n        <div class=\"google-wrapper\">\n            <div id=\"google-map\">\n                <!-- You Can Add Your Location Here Just Go To Google Map And Emped Map and past it down v -->\n                <iframe id=\"map\"\n                    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d162778.81964475298!2d-73.93891815355539!3d40.703660465248056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY!5e0!3m2!1sen!2s!4v1491335061565\"></iframe>\n            </div>\n        </div>\n    </div>\n\n    <!-- Start Footer -->\n\n    <footer>\n        <p>© Copyright 2018. </p>\n        <p>All Right Reserved By <a href=\"https://themeforest.net/user/bouthythemes/portfolio\"\n                target=\"_blank\">BouthyThemes</a></p>\n    </footer>\n\n    <!-- End Footer -->\n</section>\n"

/***/ }),

/***/ "../../../node_modules/raw-loader/index.js!./src/app/modules/portfolio/portfolio.component.html":
/*!******************************************************************************************************************!*\
  !*** /Users/apple/Documents/MIN/me/node_modules/raw-loader!./src/app/modules/portfolio/portfolio.component.html ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"work-cont\" class=\"work\">\n    <div class=\"title\">\n        <h1 class=\"kaushan\">Portfolio</h1>\n    </div>\n    <div class=\"container\">\n        <div class=\"shuffle\">\n            <ul class=\"list-unstyled\">\n                <li data-class=\"all\" class=\"active item\">All</li>\n                <li data-class=\".web\" class=\"item\">Web Design</li>\n                <li data-class=\".graphic\" class=\"item\">Graphic Design</li>\n                <li data-class=\".marketing\" class=\"item\">Marketing</li>\n            </ul>\n        </div>\n        <div class=\"gallery\">\n            <div class=\"row\">\n                <!-- Start Image Number 1 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item marketing\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Marketing</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/01.jpg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/01.jpg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 1 -->\n                <!-- ===================== -->\n                <!-- Start Image Number 2 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item graphic\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Graphic Design</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/02.jpg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/02.jpg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 2 -->\n                <!-- ===================== -->\n                <!-- Start Image Number 3 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item web\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Web Design</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/03.jpeg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/03.jpeg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 3 -->\n                <!-- ===================== -->\n                <!-- Start Image Number 4 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item marketing\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Marketing</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/04.jpg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/04.jpg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 4 -->\n                <!-- ===================== -->\n                <!-- Start Image Number 5 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item web\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Web Design</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/05.jpg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/05.jpg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 5 -->\n                <!-- ===================== -->\n                <!-- Start Image Number 6 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item graphic\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Graphic Design</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/06.jpg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/06.jpg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 6 -->\n                <!-- ===================== -->\n                <!-- Start Image Number 7 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item marketing less\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Marketing</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/07.jpg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/07.jpg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 7 -->\n                <!-- ===================== -->\n                <!-- Start Image Number 8 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item graphic less\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Graphic Design</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/08.jpg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/08.jpg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 8 -->\n                <!-- ===================== -->\n                <!-- Start Image Number 9 -->\n                <div class=\"col-lg-4 col-md-6 col-sm-12\">\n                    <div class=\"item web less\">\n                        <div class=\"over\">\n                            <div class=\"info-img\">\n                                <h4>Title Projet</h4>\n                                <span>Web Design</span>\n                            </div>\n                            <a class=\"image\" href=\"assets/images/portfolio/09.jpg\">\n                                <!-- ^ And Here -->\n                                <i class=\"fa fa-arrows-alt\"></i>\n                            </a>\n                        </div>\n                        <img src=\"assets/images/portfolio/09.jpg\" alt=\"imgPort1\" class=\"img-fluid\">\n                        <!-- Here ^ You Can Change image And Up -->\n                    </div>\n                </div>\n                <!-- End Image Number 9 -->\n                <!-- You can add more images here.. -->\n            </div>\n        </div>\n    </div>\n</section>\n"

/***/ }),

/***/ "../../../node_modules/raw-loader/index.js!./src/app/modules/services/services.component.html":
/*!****************************************************************************************************************!*\
  !*** /Users/apple/Documents/MIN/me/node_modules/raw-loader!./src/app/modules/services/services.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"serv-cont\" class=\"serv\">\n    <div class=\"title\">\n        <h1 class=\"kaushan\">Services</h1>\n    </div>\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-lg-6 col-md-12\">\n                <div class=\"servs\">\n                    <div class=\"row\">\n                        <div class=\"col-lg-6\">\n                            <div class=\"content\">\n                                <img src=\"assets/images/icons/processing.png\" alt=\"serv1\" />\n                                <h4>Web Development</h4>\n                                <p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit, sed do tempor .</p>\n                            </div>\n                        </div>\n                        <div class=\"col-lg-6\">\n                            <div class=\"content\">\n                                <img src=\"assets/images/icons/browser.png\" alt=\"serv1\" />\n                                <h4>Management</h4>\n                                <p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit, sed do tempor .</p>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-lg-6\">\n                            <div class=\"content\">\n                                <img src=\"assets/images/icons/instagram.png\" alt=\"serv1\" />\n                                <h4>Photography</h4>\n                                <p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit, sed do tempor .</p>\n                            </div>\n                        </div>\n                        <div class=\"col-lg-6\">\n                            <div class=\"content\">\n                                <img src=\"assets/images/icons/support.png\" alt=\"serv1\" />\n                                <h4>Support</h4>\n                                <p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit, sed do tempor .</p>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-lg-6 col-md-12\">\n                <div class=\"serv-vid bg-image\">\n                    <a class=\"play video\" href=\"https://www.youtube.com/watch?v=96kI8Mp1uOU\">\n                        <!-- you can edit the link here -->\n                        <i class=\"fas fa-play\"></i>\n                    </a>\n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n"

/***/ }),

/***/ "../../../node_modules/raw-loader/index.js!./src/app/modules/testimonials/testimonials.component.html":
/*!************************************************************************************************************************!*\
  !*** /Users/apple/Documents/MIN/me/node_modules/raw-loader!./src/app/modules/testimonials/testimonials.component.html ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"testim-cont\" class=\"testim\">\n    <div class=\"title\">\n        <h1 class=\"kaushan\">Testimonials</h1>\n    </div>\n    <div class=\"container\">\n        <div class=\"tabbable tabs-left\">\n            <div class=\"list-tabs\">\n                <ul class=\"nav nav-tabs\">\n                    <!-- Start Image Client 1 -->\n                    <li class=\"\">\n                        <a href=\"#a\" data-toggle=\"tab\" aria-expanded=\"false\">\n                            <div class=\"image-tab image-testim1 bg-image\"></div>\n                        </a>\n                    </li>\n                    <!-- End Image Client 1 -->\n                    <!-- Start Image Client 2 -->\n                    <li class=\"\">\n                        <a href=\"#b\" data-toggle=\"tab\" aria-expanded=\"false\">\n                            <div class=\"image-tab image-testim2 bg-image\"></div>\n                        </a>\n                    </li>\n                    <!-- End Image Client 2 -->\n                    <!-- Start Image Client 3 -->\n                    <li class=\"active\">\n                        <a href=\"#c\" data-toggle=\"tab\" aria-expanded=\"false\">\n                            <div class=\"image-tab image-testim3 bg-image\"></div>\n                        </a>\n                    </li>\n                    <!-- End Image Client 3 -->\n                    <!-- Start Image Client 4 -->\n                    <li class=\"\">\n                        <a href=\"#d\" data-toggle=\"tab\" aria-expanded=\"false\">\n                            <div class=\"image-tab image-testim4 bg-image\"></div>\n                        </a>\n                    </li>\n                    <!-- End Image Client 4 -->\n                    <!-- Start Image Client 5 -->\n                    <li class=\"\">\n                        <a href=\"#e\" data-toggle=\"tab\" aria-expanded=\"false\">\n                            <div class=\"image-tab image-testim5 bg-image\"></div>\n                        </a>\n                    </li>\n                    <!-- End Image Client 5 -->\n                </ul>\n            </div>\n            <div class=\"tab-content\">\n                <!-- Start Content Client 1 -->\n                <div class=\"tab-pane\" id=\"a\">\n                    <div class=\"profile-image\">\n                        <div class=\"image-tab image-testim1 bg-image\"></div>\n                    </div>\n                    <div class=\"profile-details\">\n                        <i class=\"fas fa-quote-left\"></i>\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut\n                            labore et dolore magna aliqua.</p>\n                        <h4>John Doe</h4>\n                        <i class=\"fas fa-quote-right\"></i>\n                    </div>\n                </div>\n                <!-- End Content Client 1 -->\n                <!-- Start Content Client 2 -->\n                <div class=\"tab-pane\" id=\"b\">\n                    <div class=\"profile-image\">\n                        <div class=\"image-tab image-testim2 bg-image\"></div>\n                    </div>\n                    <div class=\"profile-details\">\n                        <i class=\"fas fa-quote-left\"></i>\n                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque\n                            laudantium, totam rem aperiam.</p>\n                        <h4>Joshua Earle</h4>\n                        <i class=\"fas fa-quote-right\"></i>\n                    </div>\n                </div>\n                <!-- End Content Client 2 -->\n                <!-- Start Content Client 3 -->\n                <div class=\"tab-pane active\" id=\"c\">\n                    <div class=\"profile-image\">\n                        <div class=\"image-tab image-testim3 bg-image\"></div>\n                    </div>\n                    <div class=\"profile-details\">\n                        <i class=\"fas fa-quote-left\"></i>\n                        <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,\n                            sed quia non numquam.</p>\n                        <h4>Matthew Dix</h4>\n                        <i class=\"fas fa-quote-right\"></i>\n                    </div>\n                </div>\n                <!-- End Content Client 3 -->\n                <!-- Start Content Client 4 -->\n                <div class=\"tab-pane\" id=\"d\">\n                    <div class=\"profile-image\">\n                        <div class=\"image-tab image-testim4 bg-image\"></div>\n                    </div>\n                    <div class=\"profile-details\">\n                        <i class=\"fas fa-quote-left\"></i>\n                        <p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam.</p>\n                        <h4>Mike Petrucci</h4>\n                        <i class=\"fas fa-quote-right\"></i>\n                    </div>\n                </div>\n                <!-- End Content Client 4 -->\n                <!-- Start Content Client 5 -->\n                <div class=\"tab-pane\" id=\"e\">\n                    <div class=\"profile-image\">\n                        <div class=\"image-tab image-testim5 bg-image\"></div>\n                    </div>\n                    <div class=\"profile-details\">\n                        <i class=\"fas fa-quote-left\"></i>\n                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium\n                            voluptatum.</p>\n                        <h4>Lauren Dix</h4>\n                        <i class=\"fas fa-quote-right\"></i>\n                    </div>\n                </div>\n                <!-- End Content Client 5 -->\n            </div>\n        </div>\n    </div>\n    <!-- Start Client -->\n\n    <div class=\"client bg-image\">\n        <div class=\"overlay-cl\"></div>\n        <div class=\"logos\">\n            <div class=\"row\">\n                <div class=\"col-lg-2 col-md-4 col-6\">\n                    <img src=\"assets/images/clients/brand_1.png\" alt=\"brand 1\">\n                </div>\n                <div class=\"col-lg-2 col-md-4 col-6\">\n                    <img src=\"assets/images/clients/brand_2.png\" alt=\"brand 2\">\n                </div>\n                <div class=\"col-lg-3 col-md-4 col-6\">\n                    <img src=\"assets/images/clients/brand_3.png\" alt=\"brand 3\">\n                </div>\n                <div class=\"col-lg-2 col-md-4 col-6\">\n                    <img src=\"assets/images/clients/brand_4.png\" alt=\"brand 4\">\n                </div>\n                <div class=\"col-lg-2 col-md-4 col-6 last\">\n                    <img src=\"assets/images/clients/brand_1.png\" alt=\"brand 5\">\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <!-- End Client -->\n</section>"

/***/ }),

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../../../node_modules/@angular/router/fesm2015/router.js");



const routes = [];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".title h1, .type {\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9Eb2N1bWVudHMvTUlOL21lL3Jlc291cmNlcy9hc3NldHMvYW5ndWxhcjgvc3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiLCJyZXNvdXJjZXMvYXNzZXRzL2FuZ3VsYXI4L3NyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtBQ0NKIiwiZmlsZSI6InJlc291cmNlcy9hc3NldHMvYW5ndWxhcjgvc3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudGl0bGUgaDEsIC50eXBlIHtcbiAgICBjb2xvcjogd2hpdGU7XG59IiwiLnRpdGxlIGgxLCAudHlwZSB7XG4gIGNvbG9yOiB3aGl0ZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'angular8';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "../../../node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "../../../node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_navbar_navbar_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/navbar/navbar.module */ "./src/app/components/navbar/navbar.module.ts");
/* harmony import */ var _modules_about_about_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @modules/about/about.module */ "./src/app/modules/about/about.module.ts");
/* harmony import */ var _modules_contact_contact_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @modules/contact/contact.module */ "./src/app/modules/contact/contact.module.ts");
/* harmony import */ var _modules_portfolio_portfolio_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @modules/portfolio/portfolio.module */ "./src/app/modules/portfolio/portfolio.module.ts");
/* harmony import */ var _modules_services_services_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @modules/services/services.module */ "./src/app/modules/services/services.module.ts");
/* harmony import */ var _modules_testimonials_testimonials_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @modules/testimonials/testimonials.module */ "./src/app/modules/testimonials/testimonials.module.ts");
/* harmony import */ var _modules_blog_blog_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @modules/blog/blog.module */ "./src/app/modules/blog/blog.module.ts");












let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _components_navbar_navbar_module__WEBPACK_IMPORTED_MODULE_5__["NavbarModule"],
            _modules_about_about_module__WEBPACK_IMPORTED_MODULE_6__["AboutModule"],
            _modules_contact_contact_module__WEBPACK_IMPORTED_MODULE_7__["ContactModule"],
            _modules_portfolio_portfolio_module__WEBPACK_IMPORTED_MODULE_8__["PortfolioModule"],
            _modules_services_services_module__WEBPACK_IMPORTED_MODULE_9__["ServicesModule"],
            _modules_testimonials_testimonials_module__WEBPACK_IMPORTED_MODULE_10__["TestimonialsModule"],
            _modules_blog_blog_module__WEBPACK_IMPORTED_MODULE_11__["BlogModule"]
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/components/navbar/navbar.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZXNvdXJjZXMvYXNzZXRzL2FuZ3VsYXI4L3NyYy9hcHAvY29tcG9uZW50cy9uYXZiYXIvbmF2YmFyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.ts ***!
  \*******************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");


let NavbarComponent = class NavbarComponent {
    constructor() { }
    ngOnInit() {
    }
};
NavbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-navbar',
        template: __webpack_require__(/*! raw-loader!./navbar.component.html */ "../../../node_modules/raw-loader/index.js!./src/app/components/navbar/navbar.component.html"),
        styles: [__webpack_require__(/*! ./navbar.component.css */ "./src/app/components/navbar/navbar.component.css")]
    })
], NavbarComponent);



/***/ }),

/***/ "./src/app/components/navbar/navbar.module.ts":
/*!****************************************************!*\
  !*** ./src/app/components/navbar/navbar.module.ts ***!
  \****************************************************/
/*! exports provided: NavbarModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarModule", function() { return NavbarModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "../../../node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _navbar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./navbar.component */ "./src/app/components/navbar/navbar.component.ts");




let NavbarModule = class NavbarModule {
};
NavbarModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
        ],
        exports: [
            _navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"]
        ],
        declarations: [_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"]]
    })
], NavbarModule);



/***/ }),

/***/ "./src/app/modules/about/about.component.css":
/*!***************************************************!*\
  !*** ./src/app/modules/about/about.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZXNvdXJjZXMvYXNzZXRzL2FuZ3VsYXI4L3NyYy9hcHAvbW9kdWxlcy9hYm91dC9hYm91dC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/modules/about/about.component.ts":
/*!**************************************************!*\
  !*** ./src/app/modules/about/about.component.ts ***!
  \**************************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");


let AboutComponent = class AboutComponent {
    constructor() { }
    ngOnInit() {
    }
};
AboutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-about',
        template: __webpack_require__(/*! raw-loader!./about.component.html */ "../../../node_modules/raw-loader/index.js!./src/app/modules/about/about.component.html"),
        styles: [__webpack_require__(/*! ./about.component.css */ "./src/app/modules/about/about.component.css")]
    })
], AboutComponent);



/***/ }),

/***/ "./src/app/modules/about/about.module.ts":
/*!***********************************************!*\
  !*** ./src/app/modules/about/about.module.ts ***!
  \***********************************************/
/*! exports provided: AboutModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutModule", function() { return AboutModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "../../../node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _about_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./about.component */ "./src/app/modules/about/about.component.ts");




let AboutModule = class AboutModule {
};
AboutModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
        ],
        exports: [_about_component__WEBPACK_IMPORTED_MODULE_3__["AboutComponent"]],
        declarations: [_about_component__WEBPACK_IMPORTED_MODULE_3__["AboutComponent"]]
    })
], AboutModule);



/***/ }),

/***/ "./src/app/modules/blog/blog.component.css":
/*!*************************************************!*\
  !*** ./src/app/modules/blog/blog.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZXNvdXJjZXMvYXNzZXRzL2FuZ3VsYXI4L3NyYy9hcHAvbW9kdWxlcy9ibG9nL2Jsb2cuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/modules/blog/blog.component.ts":
/*!************************************************!*\
  !*** ./src/app/modules/blog/blog.component.ts ***!
  \************************************************/
/*! exports provided: BlogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlogComponent", function() { return BlogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");


let BlogComponent = class BlogComponent {
    constructor() { }
    ngOnInit() {
    }
};
BlogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-blog',
        template: __webpack_require__(/*! raw-loader!./blog.component.html */ "../../../node_modules/raw-loader/index.js!./src/app/modules/blog/blog.component.html"),
        styles: [__webpack_require__(/*! ./blog.component.css */ "./src/app/modules/blog/blog.component.css")]
    })
], BlogComponent);



/***/ }),

/***/ "./src/app/modules/blog/blog.module.ts":
/*!*********************************************!*\
  !*** ./src/app/modules/blog/blog.module.ts ***!
  \*********************************************/
/*! exports provided: BlogModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlogModule", function() { return BlogModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "../../../node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _blog_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./blog.component */ "./src/app/modules/blog/blog.component.ts");




let BlogModule = class BlogModule {
};
BlogModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
        ],
        exports: [_blog_component__WEBPACK_IMPORTED_MODULE_3__["BlogComponent"]],
        declarations: [_blog_component__WEBPACK_IMPORTED_MODULE_3__["BlogComponent"]]
    })
], BlogModule);



/***/ }),

/***/ "./src/app/modules/contact/contact.component.css":
/*!*******************************************************!*\
  !*** ./src/app/modules/contact/contact.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZXNvdXJjZXMvYXNzZXRzL2FuZ3VsYXI4L3NyYy9hcHAvbW9kdWxlcy9jb250YWN0L2NvbnRhY3QuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/modules/contact/contact.component.ts":
/*!******************************************************!*\
  !*** ./src/app/modules/contact/contact.component.ts ***!
  \******************************************************/
/*! exports provided: ContactComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactComponent", function() { return ContactComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");


let ContactComponent = class ContactComponent {
    constructor() { }
    ngOnInit() {
    }
};
ContactComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-contact',
        template: __webpack_require__(/*! raw-loader!./contact.component.html */ "../../../node_modules/raw-loader/index.js!./src/app/modules/contact/contact.component.html"),
        styles: [__webpack_require__(/*! ./contact.component.css */ "./src/app/modules/contact/contact.component.css")]
    })
], ContactComponent);



/***/ }),

/***/ "./src/app/modules/contact/contact.module.ts":
/*!***************************************************!*\
  !*** ./src/app/modules/contact/contact.module.ts ***!
  \***************************************************/
/*! exports provided: ContactModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactModule", function() { return ContactModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "../../../node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _contact_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contact.component */ "./src/app/modules/contact/contact.component.ts");




let ContactModule = class ContactModule {
};
ContactModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
        ],
        exports: [_contact_component__WEBPACK_IMPORTED_MODULE_3__["ContactComponent"]],
        declarations: [_contact_component__WEBPACK_IMPORTED_MODULE_3__["ContactComponent"]]
    })
], ContactModule);



/***/ }),

/***/ "./src/app/modules/portfolio/portfolio.component.css":
/*!***********************************************************!*\
  !*** ./src/app/modules/portfolio/portfolio.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZXNvdXJjZXMvYXNzZXRzL2FuZ3VsYXI4L3NyYy9hcHAvbW9kdWxlcy9wb3J0Zm9saW8vcG9ydGZvbGlvLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/modules/portfolio/portfolio.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/modules/portfolio/portfolio.component.ts ***!
  \**********************************************************/
/*! exports provided: PortfolioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PortfolioComponent", function() { return PortfolioComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");


let PortfolioComponent = class PortfolioComponent {
    constructor() { }
    ngOnInit() {
    }
};
PortfolioComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-portfolio',
        template: __webpack_require__(/*! raw-loader!./portfolio.component.html */ "../../../node_modules/raw-loader/index.js!./src/app/modules/portfolio/portfolio.component.html"),
        styles: [__webpack_require__(/*! ./portfolio.component.css */ "./src/app/modules/portfolio/portfolio.component.css")]
    })
], PortfolioComponent);



/***/ }),

/***/ "./src/app/modules/portfolio/portfolio.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/modules/portfolio/portfolio.module.ts ***!
  \*******************************************************/
/*! exports provided: PortfolioModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PortfolioModule", function() { return PortfolioModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "../../../node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _portfolio_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./portfolio.component */ "./src/app/modules/portfolio/portfolio.component.ts");




let PortfolioModule = class PortfolioModule {
};
PortfolioModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
        ],
        exports: [_portfolio_component__WEBPACK_IMPORTED_MODULE_3__["PortfolioComponent"]],
        declarations: [_portfolio_component__WEBPACK_IMPORTED_MODULE_3__["PortfolioComponent"]]
    })
], PortfolioModule);



/***/ }),

/***/ "./src/app/modules/services/services.component.css":
/*!*********************************************************!*\
  !*** ./src/app/modules/services/services.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZXNvdXJjZXMvYXNzZXRzL2FuZ3VsYXI4L3NyYy9hcHAvbW9kdWxlcy9zZXJ2aWNlcy9zZXJ2aWNlcy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/modules/services/services.component.ts":
/*!********************************************************!*\
  !*** ./src/app/modules/services/services.component.ts ***!
  \********************************************************/
/*! exports provided: ServicesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicesComponent", function() { return ServicesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");


let ServicesComponent = class ServicesComponent {
    constructor() { }
    ngOnInit() {
    }
};
ServicesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-services',
        template: __webpack_require__(/*! raw-loader!./services.component.html */ "../../../node_modules/raw-loader/index.js!./src/app/modules/services/services.component.html"),
        styles: [__webpack_require__(/*! ./services.component.css */ "./src/app/modules/services/services.component.css")]
    })
], ServicesComponent);



/***/ }),

/***/ "./src/app/modules/services/services.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/modules/services/services.module.ts ***!
  \*****************************************************/
/*! exports provided: ServicesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicesModule", function() { return ServicesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "../../../node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _services_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services.component */ "./src/app/modules/services/services.component.ts");




let ServicesModule = class ServicesModule {
};
ServicesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
        ],
        exports: [_services_component__WEBPACK_IMPORTED_MODULE_3__["ServicesComponent"]],
        declarations: [_services_component__WEBPACK_IMPORTED_MODULE_3__["ServicesComponent"]]
    })
], ServicesModule);



/***/ }),

/***/ "./src/app/modules/testimonials/testimonials.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/modules/testimonials/testimonials.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZXNvdXJjZXMvYXNzZXRzL2FuZ3VsYXI4L3NyYy9hcHAvbW9kdWxlcy90ZXN0aW1vbmlhbHMvdGVzdGltb25pYWxzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/modules/testimonials/testimonials.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/modules/testimonials/testimonials.component.ts ***!
  \****************************************************************/
/*! exports provided: TestimonialsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestimonialsComponent", function() { return TestimonialsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");


let TestimonialsComponent = class TestimonialsComponent {
    constructor() { }
    ngOnInit() {
    }
};
TestimonialsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-testimonials',
        template: __webpack_require__(/*! raw-loader!./testimonials.component.html */ "../../../node_modules/raw-loader/index.js!./src/app/modules/testimonials/testimonials.component.html"),
        styles: [__webpack_require__(/*! ./testimonials.component.css */ "./src/app/modules/testimonials/testimonials.component.css")]
    })
], TestimonialsComponent);



/***/ }),

/***/ "./src/app/modules/testimonials/testimonials.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/modules/testimonials/testimonials.module.ts ***!
  \*************************************************************/
/*! exports provided: TestimonialsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestimonialsModule", function() { return TestimonialsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "../../../node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _testimonials_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./testimonials.component */ "./src/app/modules/testimonials/testimonials.component.ts");




let TestimonialsModule = class TestimonialsModule {
};
TestimonialsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
        ],
        exports: [_testimonials_component__WEBPACK_IMPORTED_MODULE_3__["TestimonialsComponent"]],
        declarations: [_testimonials_component__WEBPACK_IMPORTED_MODULE_3__["TestimonialsComponent"]]
    })
], TestimonialsModule);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../../node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "../../../node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/apple/Documents/MIN/me/resources/assets/angular8/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map